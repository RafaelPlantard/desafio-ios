//
//  PullControllerKIFSpec.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/29/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import FBSnapshotTestCase
import KIF
import Nimble
import Nimble_Snapshots
import Quick
import UIColor_Hex_Swift

@testable import GitHubRepositories

/// Tests above `PullController` class.
final class PullControllerKIFSpec: QuickSpec {
    override func spec() {
        describe("the pull controller class") {
            context("the table view") {
                var indexPath: IndexPath!
                var repoTableView: UITableView!
                var pullTableView: UITableView!

                beforeEach {
                    indexPath = IndexPath(row: 0, section: 0)
                    repoTableView = self.tester().waitForView(withAccessibilityLabel: "RepoTableView") as! UITableView

                    self.tester().waitForAnimationsToFinishWithTimeout()

                    self.tester().tapRow(at: indexPath, in: repoTableView)

                    self.tester().waitForAnimationsToFinishWithTimeout()

                    pullTableView = self.tester().waitForView(withAccessibilityLabel: "PullTableView") as? UITableView
                }

                it("should has 30 items") {
                    expect(pullTableView) != nil
                    expect(pullTableView?.numberOfRows(inSection: 0)) == 30
                }

                it("should not load more items") {
                    pullTableView?.scrollTo(row: 29)

                    self.tester().waitForAnimationsToFinishWithTimeout()

                    pullTableView?.scrollTo(row: 0)

                    self.tester().waitForAnimationsToFinishWithTimeout()

                    expect(pullTableView?.numberOfRows(inSection: 0)) == 30
                }

                context("the prototype cell") {
                    var v: PullView?

                    beforeEach {
                        pullTableView?.scrollTo(row: 10)

                        self.tester().waitForAnimationsToFinishWithTimeout()

                        v = pullTableView?.cellFor(row: 10) as? PullView
                    }

                    it("should contain correct parsed values") {
                        expect(v?.titleLabel.text) == "Clarify summary description"
                        expect(v?.bodyLabel.text) == "Resolves https://github.com/elastic/elasticsearch/issues/23793"
                        expect(v?.profileImageView.image) != nil
                        expect(v?.usernameLabel.text) == "GlenRSmith"
                        expect(v?.dateLabel.text) == "Mar 29, 2017"
                    }

                    it("should respect snapshot") {
                        expect(v).to(haveValidSnapshot())
                    }

                    it("should contain correct UI") {
                        expect(v?.titleLabel.textColor.hexString(false)) == "#2876A7"
                        expect(v?.bodyLabel.textColor.hexString(false)) == "#000000"
                        expect(v?.profileImageView).to(haveValidSnapshot())
                        expect(v?.usernameLabel.textColor.hexString(false)) == v?.titleLabel.textColor.hexString(false)
                        expect(v?.dateLabel.textColor.hexString(false)) == "#C2C2C2"
                    }

                    it("should be tappable") {
                        v?.tap()
                        
                        self.tester().tap(row: 10, in: pullTableView)

                        self.tester().waitForAnimationsToFinish()
                    }
                }

                afterEach {
                    self.tester().tapView(withAccessibilityLabel: "Back")
                }
            }
        }
    }
}
