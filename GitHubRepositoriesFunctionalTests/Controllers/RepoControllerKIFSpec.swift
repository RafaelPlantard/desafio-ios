//
//  RepoControllerKIFSpec.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/29/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import FBSnapshotTestCase
import KIF
import Nimble
import Nimble_Snapshots
import Quick
import UIColor_Hex_Swift

@testable import GitHubRepositories

/// Tests above `PullController` class.
final class RepoControllerKIFSpec: QuickSpec {
    override func spec() {
        describe("the repo controller class") {
            context("the table view") {
                var tableView: UITableView?

                beforeEach {
                    self.tester().waitForAnimationsToFinishWithTimeout()

                    tableView = self.tester().waitForView(withAccessibilityLabel: "RepoTableView") as? UITableView
                }

                it("should has 30 elements loaded") {
                    expect(tableView) != nil
                    expect(tableView?.numberOfRows) == 30
                }

                it("should has 60 elements loaded") {
                    tableView?.scrollTo(row: 29)

                    self.tester().waitForAnimationsToFinishWithTimeout()

                    tableView?.scrollTo(row: 36)

                    expect(tableView?.numberOfRows) == 60

                    self.tester().waitForAnimationsToFinishWithTimeout()

                    tableView?.scrollTo(row: 0)

                    self.tester().waitForAnimationsToFinishWithTimeout()
                }

                it("should contain unique elements loaded") {
                    let firstElement = tableView?.cellFor(row: 0) as? RepoView

                    tableView?.scrollTo(row: 30)

                    self.tester().waitForAnimationsToFinishWithTimeout()

                    let thirtyFirstElement = tableView?.cellFor(row: 30) as? RepoView

                    expect(firstElement?.nameLabel.text) != nil
                    expect(thirtyFirstElement?.nameLabel.text) != nil
                    expect(firstElement?.nameLabel.text) != thirtyFirstElement?.nameLabel.text

                    tableView?.scrollTo(row: 0)

                    self.tester().waitForAnimationsToFinishWithTimeout()
                }

                context("the prototype cell") {
                    var v: RepoView?

                    beforeEach {
                        v = tableView?.cellFor(row: 1) as? RepoView
                    }

                    it("should contain correct parsed values") {
                        expect(v?.nameLabel.text) == "elasticsearch"
                        expect(v?.descriptionLabel.text) == "Open Source, Distributed, RESTful Search Engine"
                        expect(v?.forksLabel.text) == "7219"
                        expect(v?.starsLabel.text) == "21727"
                        expect(v?.usernameLabel.text) == "elastic"
                        expect(v?.fullNameLabel.text) == "elastic/elasticsearch"
                        expect(v?.profileImageView.image) != nil
                    }

                    it("should respect snapshot") {
                        expect(v).to(haveValidSnapshot())
                    }

                    it("should contain correct UI") {
                        expect(v?.nameLabel.textColor.hexString(false)) == "#2876A7"
                        expect(v?.descriptionLabel.textColor.hexString(false)) == "#000000"
                        expect(v?.forksLabel.textColor.hexString(false)) == "#DD9223"
                        expect(v?.forksLabel.textColor.hexString(false)) == v?.starsLabel.textColor.hexString(false)
                        expect(v?.usernameLabel.textColor.hexString(false)) == v?.nameLabel.textColor.hexString(false)
                        expect(v?.fullNameLabel.textColor.hexString(false)) == "#C2C2C2"

                        expect(v?.profileImageView).to(haveValidSnapshot())
                    }
                }
            }
        }
    }
}

