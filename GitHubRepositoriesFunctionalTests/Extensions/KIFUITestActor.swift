//
//  KIFUITestActor.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/29/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import struct Foundation.IndexPath
import class KIF.KIFUITestActor
import class UIKit.UITableView

extension KIFUITestActor {
    func waitForAnimationsToFinishWithTimeout() {
        waitForAnimationsToFinish(withTimeout: 3.15)
    }

    func tap(row: Int, section: Int = 0, in tableView: UITableView!) {
        let indexPath = IndexPath(row: row, section: section)

        tapRow(at: indexPath, in: tableView)
    }
}
