//
//  UITableView.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/29/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import class UIKit.UITableView
import class UIKit.UITableViewCell
import enum UIKit.UITableViewScrollPosition
import struct Foundation.IndexPath

extension UITableView {
    var numberOfRows: Int {
        return numberOfRows(inSection: 0)
    }
}

// MARK: Functions

extension UITableView {
    func cellFor(row: Int, section: Int = 0) -> UITableViewCell? {
        let indexPath = IndexPath(row: row, section: section)

        return cellForRow(at: indexPath)
    }

    func scrollTo(row: Int, section: Int = 0, direction: UITableViewScrollPosition = .bottom) {
        let direction: UITableViewScrollPosition = (row == 0) ? .top : direction
        let indexPath = IndexPath(row: row, section: section)

        scrollToRow(at: indexPath, at: direction, animated: true)
    }
}
