//
//  RepoView.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/26/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import UIKit

final class RepoView: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var forksLabel: UILabel!
    @IBOutlet var starsLabel: UILabel!
    @IBOutlet var profileImageView: UIImageView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var fullNameLabel: UILabel!
}

// MARK: RepoViewable

extension RepoView: RepoViewable { }
