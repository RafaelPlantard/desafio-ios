//
//  RepoViewModel.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/26/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import struct RxCocoa.Driver

struct RepoViewModel {
    let repos: Driver<[Repo]>
    let showPulls: Driver<Void>
    let title: Driver<String>

    init(input: (modelSelected: Driver<Repo>, nextPageTrigger: Driver<Void>, viewWillAppear: Driver<Void>),
         dependency: GitHubAPI) {
        let repoSelected = input.modelSelected
        let nextPageTrigger = input.nextPageTrigger
        let viewWillAppear = input.viewWillAppear
        let trigger = nextPageTrigger.throttle(0.35).asObservable()

        repos = dependency.getRepos(nextPageTrigger: trigger).asDriver(onErrorJustReturn: [])

        showPulls = repoSelected.flatMapLatest({ repo in
            dependency.selectedRepo.onNext(repo)

            return Driver.just(())
        })

        title = viewWillAppear.map({ "\(EnvironmentVariable.mainTitle)" })
    }
}
