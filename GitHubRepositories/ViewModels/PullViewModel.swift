//
//  PullViewModel.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/26/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import RxCocoa
import RxSwift

struct PullViewModel {
    let pulls: Driver<[Pull]>
    let repoName: Driver<String>
    let repoURL: Driver<URL>
    let backButtonTitle: Driver<String>

    init(input: (modelSelected: Driver<Pull>, viewWillAppear: Driver<Void>), dependencies: GitHubAPI) {
        let modelSelected = input.modelSelected
        let viewWillAppear = input.viewWillAppear

        backButtonTitle = viewWillAppear.flatMapLatest({
            Driver.just("")
        })

        pulls = dependencies.selectedRepo.flatMapLatest(dependencies.getPulls).asDriver(onErrorJustReturn: [])

        repoName = pulls.flatMapFirst({ pulls in
            guard !pulls.isEmpty, let pull = pulls.first else { return Driver.empty() }

            return Driver.just(pull.repoName)
        })

        repoURL = modelSelected.map({ $0.url })
    }
}
