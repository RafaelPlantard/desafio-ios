//
//  JSONStub.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/26/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import JSONStub

enum JSONStub: String {
    case badCredentials
    case elasticSearch
    case pullsResponse
    case reposResponse
    case reposSecondResponse
    case retrofit
    case rxJava
    case rxJavaPulls
}

// MARK: FileStub conforms

extension JSONStub: FileStub {
    var fileName: String {
        return "\(rawValue.pascalCase).json"
    }
}
