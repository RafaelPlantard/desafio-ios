//
//  GitHubAPI.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/24/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import class RxSwift.Observable
import class RxSwift.BehaviorSubject

protocol GitHubAPI {
    var selectedRepo: BehaviorSubject<Repo> { get }

    func getRepos(nextPageTrigger: Observable<Void>) -> Observable<[Repo]>
    func getPulls(repo: Repo) -> Observable<[Pull]>
}
