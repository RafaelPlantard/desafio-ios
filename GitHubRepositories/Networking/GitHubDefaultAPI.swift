//
//  GitHubDefaultAPI.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/24/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import class Foundation.ProcessInfo
import class Moya.RxMoyaProvider
import class Moya.NetworkActivityPlugin
import enum Moya.StubBehavior
import Moya_ObjectMapper
import class PKHUD.HUD
import class RxSwift.BehaviorSubject
import class RxSwift.Observable

final class GitHubDefaultAPI {
    let provider = RxMoyaProvider<GitHubProvider>(stubClosure: { _ in
            ProcessInfo.isInTestMode ? .delayed(seconds: EnvironmentVariable.baseDelay.double) : .never
        },
                                                  manager: .default, plugins: [
        NetworkActivityPlugin(networkActivityClosure: { type in
            switch type {
            case .began: HUD.show(.progress)
            case .ended: HUD.hide(afterDelay: EnvironmentVariable.baseDelay.double)
            }
        })
    ])

    // MARK: GitHubAPI conforms

    let selectedRepo = BehaviorSubject(value: Repo.default)

    // MARK: Primitive variables

    var repoNextPage: Int = 1
    var repoTotalPages: Int = 0
    var pullsPage: Int = 1
}

// MARK: Singleton implementation

extension GitHubDefaultAPI {
    static let `default` = GitHubDefaultAPI()
}

// MARK: GitHubAPI conforms

extension GitHubDefaultAPI: GitHubAPI {
    func getRepos(nextPageTrigger: Observable<Void>) -> Observable<[Repo]> {
        return paginateRepoItems(page: repoNextPage, nextPageTrigger: nextPageTrigger).scan([]) { accum, page in
            return accum + page.item
        }
    }

    func getPulls(repo: Repo) -> Observable<[Pull]> {
        let request: GitHubProvider = .getPulls(repo.fullName, pullsPage)

        return provider.request(request)
            .filterSuccessfulStatusCodes()
            .mapArray(Pull.self)
    }
}

// MARK: Private functions

private extension GitHubDefaultAPI {
    func paginateRepoItems(page: Int, nextPageTrigger: Observable<Void>) -> Observable<Page<[Repo]>> {
        let request: GitHubProvider = .getRepos(page)

        return provider.request(request)
            .filterSuccessfulStatusCodes()
            .showHUDOnError()
            .mapObject(Result<Repo>.self)
            .do(onNext: {
                self.repoTotalPages = $0.totalCount / 30
                self.repoNextPage += 1
            })
            .map({ $0.items })
            .catchErrorJustReturn([])
            .map({ Page<[Repo]>(item: $0, page: self.repoNextPage) })
            .paginate(nextPageTrigger: nextPageTrigger, hasNextPage: { page -> Bool in
                return page.page < self.repoTotalPages
            }, nextPageFactory: { [weak self] page -> Observable<Page<[Repo]>> in
                self?.paginateRepoItems(page: page.page, nextPageTrigger: nextPageTrigger) ?? Observable.empty()
            })
    }
}
