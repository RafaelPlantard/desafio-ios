//
//  GitHubProvider.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/24/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import Moya

enum GitHubProvider {
    case getRepos(Int)
    case getPulls(String, Int)
}

// MARK: TargetType conforms

extension GitHubProvider: TargetType {
    var baseURL: URL {
        return EnvironmentVariable.baseURL.url
    }

    var path: String {
        switch self {
        case .getRepos: return "search/repositories"
        case .getPulls(let fullName, _): return "repos/\(fullName)/pulls"
        }
    }

    var method: Method {
        return .get
    }

    var parameters: [String: Any]? {
        switch self {
        case .getRepos(let page):
            return [
                GitHubParamKeys.query: "language:Java",
                GitHubParamKeys.sort: "stars",
                GitHubParamKeys.page: page
            ]

        case .getPulls(_, let page):
            return [
                GitHubParamKeys.state: "all",
                GitHubParamKeys.page: page
            ]
        }
    }

    var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }

    var sampleData: Data {
        guard let data = stub.fileName.fileData() else { fatalError("JSON file cannot be nil") }

        return data
    }

    var task: Task {
        return .request
    }
}

// MARK: Private computed variables

private extension GitHubProvider {
    var stub: JSONStub {
        switch self {
        case .getPulls: return JSONStub.pullsResponse
        case .getRepos(let page):
            return page < 2 ? JSONStub.reposResponse : JSONStub.reposSecondResponse
        }
    }
}
