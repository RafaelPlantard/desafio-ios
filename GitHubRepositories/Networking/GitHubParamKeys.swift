//
//  GitHubParamKeys.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/28/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

struct GitHubParamKeys {
    static let page = "page"
    static let query = "q"
    static let state = "state"
    static let sort = "sort"
}
