//
//  Result.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/26/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import protocol ObjectMapper.ImmutableMappable
import class ObjectMapper.Map

struct Result<T: ImmutableMappable>: ImmutableMappable {
    let totalCount: Int
    let items: [T]

    // MARK: ImmutableMappable conforms

    init(map: Map) throws {
        totalCount = try map.value("total_count")
        items = try map.value("items")
    }
}
