//
//  Response.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/29/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import ObjectMapper

struct Response: ImmutableMappable {
    let documentationURL: URL
    let message: String

    // MARK: ImmutableMappable conforms

    init(map: Map) throws {
        documentationURL = try map.value("documentation_url", using: URLTransform())
        message = try map.value("message")
    }
}
