//
//  Pull.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/24/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import protocol ObjectMapper.ImmutableMappable
import class ObjectMapper.ISO8601DateTransform
import class ObjectMapper.Map
import class ObjectMapper.URLTransform
import struct Foundation.Date
import struct Foundation.URL

struct Pull: ImmutableMappable {
    let author: String
    let profilePicture: URL
    let title: String
    let date: Date
    let body: String
    let url: URL
    let repoName: String

    // MARK: ImmutableMappable conforms

    init(map: Map) throws {
        author = try map.value("user.login")
        profilePicture = try map.value("user.avatar_url", using: URLTransform())
        title = try map.value("title")
        date = try map.value("created_at", using: ISO8601DateTransform())
        body = try map.value("body")
        url = try map.value("html_url", using: URLTransform())
        repoName = try map.value("base.repo.name")
    }
}
