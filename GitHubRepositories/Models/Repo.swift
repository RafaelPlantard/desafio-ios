//
//  Repo.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/24/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import protocol ObjectMapper.ImmutableMappable
import class ObjectMapper.Map
import class ObjectMapper.URLTransform
import struct Foundation.URL

struct Repo: ImmutableMappable {
    let name: String
    let description: String
    let author: String
    let profilePicture: URL
    let stars: Int
    let forks: Int
    let fullName: String

    // MARK: Non-argument initializer

    init() {
        name = ""
        description = EnvironmentVariable.noDescriptionMessage.description
        author = ""
        profilePicture = EnvironmentVariable.profileURL.url
        stars = 0
        forks = 0
        fullName = ""
    }

    // MARK: ImmutableMappable conforms

    init(map: Map) throws {
        name = try map.value("name")
        author = try map.value("owner.login")
        profilePicture = try map.value("owner.avatar_url", using: URLTransform())
        stars = try map.value("stargazers_count")
        forks = try map.value("forks_count")
        fullName = try map.value("full_name")

        do {
            description = try map.value("description")
        } catch {
            description = EnvironmentVariable.noDescriptionMessage.description
        }
    }
}

// MARK: Singleton implementation

extension Repo {
    static let `default` = Repo()
}

// MARK: Equatable conforms

extension Repo: Equatable {
    public static func == (lhs: Repo, rhs: Repo) -> Bool {
        let isNamesEqual = lhs.name == rhs.name && lhs.author == rhs.author && lhs.fullName == rhs.fullName
        let isDescriptionEqual = lhs.description == rhs.description
        let isAvatarEqual = lhs.profilePicture == rhs.profilePicture
        let isStarsEqual = lhs.stars == rhs.stars
        let isForksEqual = lhs.forks == rhs.forks

        return isNamesEqual && isDescriptionEqual && isAvatarEqual && isStarsEqual && isForksEqual
    }
}
