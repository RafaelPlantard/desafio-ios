//
//  Page.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/28/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

struct Page<Element>: PageType {
    typealias T = Element

    let item: Element
    let page: Int
}
