//
//  EnvironmentVariable.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/26/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import class Foundation.Bundle
import struct Foundation.URL

enum EnvironmentVariable: String {
    case accessToken = "ACCESS_TOKEN"
    case baseURL = "BASE_URL"
    case noDescriptionMessage = "NO_DESCRIPTION_MESSAGE"
    case profileURL = "PROFILE_URL"
    case serviceError = "SERVICE_ERROR"
    case mainTitle = "MAIN_TITLE"
    case baseDelay = "BASE_DELAY"
}

// MARK: CustomStringConvertible conforms

extension EnvironmentVariable: CustomStringConvertible {
    var description: String {
        let node = String(describing: EnvironmentVariable.self)

        guard let keys = Bundle.main.infoDictionary?[node] as? [String: String], let value = keys[rawValue] else {
            fatalError("We can't find a value for \(rawValue) key")
        }

        return value
    }
}

// MARK: Computed variables

extension EnvironmentVariable {
    var url: URL {
        guard let url = URL(string: description) else { fatalError("We can't translate the \(rawValue) value to URL") }

        return url
    }

    var double: Double {
        guard let double = Double(description) else { fatalError("The \(rawValue) isn't a Double value") }

        return double
    }
}
