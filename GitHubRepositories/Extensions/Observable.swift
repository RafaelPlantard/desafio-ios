//
//  Observable.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/29/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import class PKHUD.HUD
import enum PKHUD.HUDContentType
import class RxSwift.Observable
import enum Moya.MoyaError
import Moya_ObjectMapper

extension Observable {
    func showHUDOnError() -> Observable<E> {
        return self.do(onError: { error in
            var message = HUDContentType.labeledError(title: "Ops", subtitle: "\(EnvironmentVariable.serviceError)")

            if let moya = error as? MoyaError, case .statusCode(let response) = moya,
                let object = try? response.mapObject(Response.self) {
                message = .labeledError(title: "API", subtitle: object.message)
            }

            HUD.show(message)
        })
    }
}
