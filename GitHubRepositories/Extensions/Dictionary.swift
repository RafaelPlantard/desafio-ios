//
//  Dictionary.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/29/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

func + <T, U>(lhs: [T: U], rhs: [T: U]) -> [T: U] {
    var lhsTemp = lhs

    for (key, value) in rhs {
        lhsTemp[key] = value
    }

    return lhsTemp
}
