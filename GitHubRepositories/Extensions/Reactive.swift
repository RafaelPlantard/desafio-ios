//
//  Reactive.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/26/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import class Foundation.ProcessInfo
import struct Foundation.Selector
import struct Foundation.URL
import class PKHUD.HUD
import class RxCocoa.UIBindingObserver
import struct RxSwift.Reactive
import class RxSwift.Observable
import class UIKit.UIApplication
import class UIKit.UIBarButtonItem
import class UIKit.UIViewController

extension Reactive where Base: UIViewController {
    var backTitle: UIBindingObserver<Base, String> {
        return UIBindingObserver(UIElement: base) { element, value in
            element.navigationController?.navigationBar.backItem?.title = value
        }
    }

    var openURL: UIBindingObserver<Base, URL> {
        return UIBindingObserver(UIElement: base) { _, url in
            guard !ProcessInfo.isInTestMode else {
                return HUD.flash(.labeledError(title: "In Test Mode", subtitle: "In normal conditions opens Safari"),
                                 delay: EnvironmentVariable.baseDelay.double)
            }

            _ = UIApplication.shared.openURL(url)
        }
    }

    var viewWillAppear: Observable<Void> {
        return sentMessage(.viewWillAppear).mapTo(())
    }
}

// MARK: Private Selector extensions

extension Selector {
    static let viewWillAppear = #selector(UIViewController.viewWillAppear(_:))
}
