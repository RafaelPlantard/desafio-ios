//
//  ObservableType.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/28/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import class RxSwift.Observable
import protocol RxSwift.ObservableType

extension ObservableType {
    func mapTo<R>(_ value: R) -> Observable<R> {
        return map({ _ in value })
    }

    func paginate<O: ObservableType>(nextPageTrigger: O, hasNextPage: @escaping (E) -> Bool,
                                     nextPageFactory: @escaping (E) -> Observable<E>) -> Observable<E> {
        return flatMapLatest({ page -> Observable<E> in
            guard hasNextPage(page) else { return Observable.just(page) }

            return Observable.concat([
                                         Observable.just(page),
                                         Observable.never().takeUntil(nextPageTrigger),
                                         nextPageFactory(page)
                                     ])
        })
    }
}
