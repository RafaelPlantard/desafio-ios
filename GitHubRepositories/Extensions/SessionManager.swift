//
//  SessionManager.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/29/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import class Alamofire.SessionManager
import class Foundation.URLSessionConfiguration

extension SessionManager {
    static let `default`: SessionManager = {
        let configuration = URLSessionConfiguration.default
        let authorizationHeader: [String: String] = ["Authorization": "token \(EnvironmentVariable.accessToken)"]

        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders + authorizationHeader

        let manager = SessionManager(configuration: configuration)

        manager.startRequestsImmediately = false

        return manager
    }()
}
