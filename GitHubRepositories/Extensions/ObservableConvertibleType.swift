//
//  ObservableConvertibleType.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/28/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import protocol RxSwift.ObservableConvertibleType
import struct RxCocoa.Driver

extension ObservableConvertibleType where E == Void {
    func asDriver() -> Driver<E> {
        return asDriver(onErrorJustReturn: ())
    }
}
