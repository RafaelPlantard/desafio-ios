//
//  ProcessInfo.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/30/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import class Foundation.ProcessInfo

extension ProcessInfo {
    static let isInTestMode = ProcessInfo.processInfo.environment["XCInjectBundleInto"] != nil
}
