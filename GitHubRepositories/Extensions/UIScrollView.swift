//
//  UIScrollView.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/26/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import UIKit

extension UIScrollView {
    var isNearBottomEdge: Bool {
        let edgeOffset: CGFloat = 20

        return contentOffset.y + frame.size.height + edgeOffset > contentSize.height
    }
}
