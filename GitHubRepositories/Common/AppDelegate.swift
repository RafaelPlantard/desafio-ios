//
//  AppDelegate.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/24/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import UIColor_Hex_Swift
import UIKit

@UIApplicationMain
final class AppDelegate: UIResponder {
    var window: UIWindow?
}

// MARK: UIApplicationDelegate conforms

extension AppDelegate: UIApplicationDelegate {
    func applicationDidFinishLaunching(_ application: UIApplication) {
        let navigationBarAppearance = UINavigationBar.appearance()

        navigationBarAppearance.tintColor = UIColor.white
        navigationBarAppearance.barStyle = .blackOpaque
    }
}
