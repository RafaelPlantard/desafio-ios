//
//  PullController.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/26/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import RxDataSources
import class RxSwift.DisposeBag
import class RxSwift.MainScheduler

final class PullController: UIViewController {
    @IBOutlet var tableView: UITableView!

    // MARK: Rx

    let disposeBag = DisposeBag()
}

// MARK: InfiniteLoadable conforms

extension PullController: InfiniteLoadable { }

// MARK: UIViewController functions

extension PullController {
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpRx()
    }
}

// MARK: Private functions

private extension PullController {
    func setUpRx() {
        let id = String(describing: PullView.self)
        let viewModel = PullViewModel(input: (modelSelected: tableView.rx.modelSelected(Pull.self).asDriver(),
                                              viewWillAppear: rx.viewWillAppear.asDriver()),
                                      dependencies: GitHubDefaultAPI.default)

        viewModel.pulls.drive(tableView.rx.items(cellIdentifier: id, cellType: PullView.self)) { _, pull, cell in
            cell.setUp(pull: pull)
        }.disposed(by: disposeBag)

        viewModel.backButtonTitle.drive(rx.backTitle).disposed(by: disposeBag)
        viewModel.repoName.drive(rx.title).disposed(by: disposeBag)
        viewModel.repoURL.drive(rx.openURL).disposed(by: disposeBag)

        tableView.rx.itemSelected.delaySubscription(0.7, scheduler: MainScheduler.instance)
            .map({ ($0, true) }).subscribe(onNext: tableView.deselectRow).disposed(by: disposeBag)
    }
}
