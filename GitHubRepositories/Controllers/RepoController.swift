//
//  RepoController.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/24/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import RxDataSources
import struct RxCocoa.Driver
import class RxSwift.DisposeBag
import class RxSwift.MainScheduler
import class UIKit.UITableView

final class RepoController: UIViewController {
    @IBOutlet var tableView: UITableView!

    // MARK: Rx

    let disposeBag = DisposeBag()
}

// MARK: InfiniteLoadable conforms

extension RepoController: InfiniteLoadable { }

// MARK: UIViewController functions

extension RepoController {
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpRx()
    }
}

// MARK: Private functions

private extension RepoController {
    func setUpRx() {
        let id = String(describing: RepoView.self)

        let viewModel = RepoViewModel(input: (modelSelected: tableView.rx.modelSelected(Repo.self).asDriver(),
                                              nextPageTrigger: nextPageTrigger.asDriver(),
                                              viewWillAppear: rx.viewWillAppear.asDriver()),
                                      dependency: GitHubDefaultAPI.default)

        viewModel.repos.drive(tableView.rx.items(cellIdentifier: id, cellType: RepoView.self)) { _, repo, cell in
            cell.setUp(repo: repo)
        }.disposed(by: disposeBag)

        viewModel.showPulls.map({ ("showPulls", nil) }).do(onNext: performSegue).drive().disposed(by: disposeBag)
        viewModel.title.drive(rx.title).disposed(by: disposeBag)

        tableView.rx.itemSelected.delaySubscription(0.7, scheduler: MainScheduler.instance)
            .map({ ($0, true) }).subscribe(onNext: tableView.deselectRow).disposed(by: disposeBag)
    }
}
