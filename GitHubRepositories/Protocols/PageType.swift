//
//  PageType.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/28/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

protocol PageType {
    associatedtype T

    var item: T { get }
    var page: Int { get }
}
