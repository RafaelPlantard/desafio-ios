//
//  RepoViewable.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/26/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import AlamofireImage
import class UIKit.UILabel
import class UIKit.UIImageView

protocol RepoViewable {
    var nameLabel: UILabel! { get set }
    var descriptionLabel: UILabel! { get set }
    var forksLabel: UILabel! { get set }
    var starsLabel: UILabel! { get set }
    var profileImageView: UIImageView! { get set }
    var usernameLabel: UILabel! { get set }
    var fullNameLabel: UILabel! { get set }

    func setUp(repo: Repo)
}

extension RepoViewable {
    func setUp(repo: Repo) {
        nameLabel.text = repo.name
        descriptionLabel.text = repo.description
        forksLabel.text = repo.forks.description
        starsLabel.text = repo.stars.description
        profileImageView.af_setImage(withURL: repo.profilePicture, placeholderImage: #imageLiteral(resourceName: "GitHubLight"))
        usernameLabel.text = repo.author
        fullNameLabel.text = repo.fullName
    }
}
