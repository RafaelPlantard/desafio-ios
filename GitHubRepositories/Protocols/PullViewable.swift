//
//  PullViewable.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/26/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import AlamofireImage
import SwiftDate
import UIKit

protocol PullViewable {
    var titleLabel: UILabel! { get set }
    var bodyLabel: UILabel! { get set }
    var profileImageView: UIImageView! { get set }
    var usernameLabel: UILabel! { get set }
    var dateLabel: UILabel! { get set }
    var dateRegion: DateInRegion { get }

    func setUp(pull: Pull)
}

extension PullViewable {
    func setUp(pull: Pull) {
        titleLabel.text = pull.title
        bodyLabel.text = pull.body
        profileImageView.af_setImage(withURL: pull.profilePicture, placeholderImage: #imageLiteral(resourceName: "GitHubLight"))
        usernameLabel.text = pull.author
        dateLabel.text = pull.date.string(dateStyle: .medium, timeStyle: .none, in: dateRegion.region)
    }
}
