//
//  InfiniteLoadable.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/26/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import class UIKit.UITableView
import class RxSwift.Observable

protocol InfiniteLoadable {
    var tableView: UITableView! { get set }
    var nextPageTrigger: Observable<Void> { get }
}

extension InfiniteLoadable {
    var nextPageTrigger: Observable<Void> {
        return tableView.rx.contentOffset.flatMapLatest({ _ in
            return self.tableView.isNearBottomEdge ? Observable.just(()) : Observable.empty()
        })
    }
}
