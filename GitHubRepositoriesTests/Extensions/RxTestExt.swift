//
//  EquatableRecordedEventArray.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/30/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import RxTest

struct EquatableRecordedEventArray<T: Equatable>: Equatable {
    let wrapee: [Recorded<Event<[T]>>]
}

struct EquatableRecordedEvent<T: Equatable>: Equatable {
    let wrapee: [Recorded<Event<T>>]
}

func == <T: Equatable>(lhs: EquatableRecordedEvent<T>, rhs: EquatableRecordedEvent<T>) -> Bool {
    return lhs.wrapee == rhs.wrapee
}

func == <Element: Equatable>(lhs: Event<[Element]>, rhs: Event<[Element]>) -> Bool {
    switch (lhs, rhs) {
    case (.completed, .completed): return true
    case (.error(let e1), .error(let e2)):
        // if the references are equal, then it's the same object
        let lhsObject = lhs as AnyObject
        let rhsObject = rhs as AnyObject

        if lhsObject === rhsObject {
                return true
        }

        #if os(Linux)
            return "\(e1)" == "\(e2)"
        #else
            let error1 = e1 as NSError
            let error2 = e2 as NSError

            return error1.domain == error2.domain
            && error1.code == error2.code
            && "\(e1)" == "\(e2)"
        #endif
    case (.next(let v1), .next(let v2)): return v1 == v2
    default: return false
    }
}

func == <T: Equatable>(lhs: [Recorded<Event<[T]>>], rhs: [Recorded<Event<[T]>>]) -> Bool {
    if lhs.count != rhs.count {
        return false
    }
    for (index, element) in lhs.enumerated() {
        if !(element == rhs[index]) {
            return false
        }
    }
    return true
}

func == <T: Equatable>(lhs: [Recorded<Event<T>>], rhs: [Recorded<Event<T>>]) -> Bool {
    if lhs.count != rhs.count {
        return false
    }
    for (index, element) in lhs.enumerated() {
        if !(element == rhs[index]) {
            return false
        }
    }
    return false
}

func == <T: Equatable>(lhs: Recorded<Event<[T]>>, rhs: Recorded<Event<[T]>>) -> Bool {
    return lhs.time == rhs.time && lhs.value == rhs.value
}

func == <T: Equatable>(lhs: EquatableRecordedEventArray<T>, rhs: EquatableRecordedEventArray<T>) -> Bool {
    return lhs.wrapee == rhs.wrapee
}

extension Array where Element: Equatable {
    static func == <T: Equatable>(lhs: [T], rhs: [T]) -> Bool {
        if lhs.count != rhs.count {
            return false
        }

        for (key, value) in lhs.enumerated() {
            if value != rhs[key] {
                return false
            }
        }

        return true
    }
}
