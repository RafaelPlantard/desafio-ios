//
//  ResponseSpec.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/30/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import JSONStub
import Nimble
import ObjectMapper
import Quick

@testable import GitHubRepositories

/// Tests above `Response` class.
final class ResponseSpec: QuickSpec {
    override func spec() {
        describe("the response struct") { 
            var jsonString: String!
            var object: Response?

            beforeEach {
                jsonString = JSONStub.badCredentials.fileName.fileString()
                object = try? Response(JSONString: jsonString)
            }

            it("should parse correctly data") {
                expect(object).toNot(beNil())
                expect(object?.message) == "Bad credentials"
                expect(object?.documentationURL.absoluteString) == "https://developer.github.com/v3"
            }
        }
    }
}
