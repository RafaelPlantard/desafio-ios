//
//  RepoViewModelSpec.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/30/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import Nimble
import Quick
import RxCocoa
import RxSwift
import RxTest

@testable import GitHubRepositories

/// Tests above `RepoViewModel` class.
final class RepoViewModelSpec: QuickSpec {
    override func spec() {
        describe("the repo view model class") {
            it("should emits correct sections") {
                let time = TestScheduler(initialClock: 0, simulateProcessingDelay: false)

                driveOnScheduler(time) {
                    let paging: [Recorded<Event<Void>>] = [
                        completed(0)
                    ]

                    let viewModel = RepoViewModel(input: (modelSelected: Driver.empty(),
                                                          nextPageTrigger: time.createHotObservable(paging).asDriver(),
                                                          viewWillAppear: Driver.empty()),
                                                  dependency: GitHubMockAPI.default)

                    let observer = time.createObserver([Repo].self)

                    _ = viewModel.repos.drive(observer)

                    time.start()

                    let correct = [
                        next(0, GitHubMockAPI.sampleRepos)
                    ]

                    let wrappedResultRecords = EquatableRecordedEventArray(wrapee: observer.events)
                    let wrappedExpectedResults = EquatableRecordedEventArray(wrapee: correct)

                    expect(wrappedExpectedResults) == wrappedResultRecords 
                }
            }
        }
    }
}
