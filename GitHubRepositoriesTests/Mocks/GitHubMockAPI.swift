//
//  GitHubMockAPI.swift
//  GitHubRepositories
//
//  Created by Rafael Ferreira on 3/30/17.
//  Copyright © 2017 Swift Yah. All rights reserved.
//

import class RxSwift.BehaviorSubject
import class RxSwift.Observable
import ObjectMapper

@testable import GitHubRepositories

struct GitHubMockAPI: GitHubAPI {
    let selectedRepo = BehaviorSubject(value: Repo.default)

    func getPulls(repo: Repo) -> Observable<[Pull]> {
        let pulls = GitHubMockAPI.samplePulls

        return Observable.just(pulls)
    }

    func getRepos(nextPageTrigger: Observable<Void>) -> Observable<[Repo]> {
        return paginateRepoItems(page: 1, nextPageTrigger: nextPageTrigger).scan([], accumulator: { $0 + $1 })
    }
}

// MARK: Singleton implementation

extension GitHubMockAPI {
    static let `default` = GitHubMockAPI()
}

// MARK: Static constants

extension GitHubMockAPI {
    static let sampleRepos = [
        Repo(fromStub: JSONStub.rxJava)!,
        Repo(fromStub: JSONStub.elasticSearch)!,
        Repo(fromStub: JSONStub.retrofit)!
    ]

    static let samplePulls = try! Mapper<Pull>().mapArray(JSONString: JSONStub.rxJavaPulls.fileName.fileString())
}

// MARK: File private functions

fileprivate extension GitHubMockAPI {
    func paginateRepoItems(page: Int, nextPageTrigger: Observable<Void>) -> Observable<[Repo]> {
        let repos = GitHubMockAPI.sampleRepos

        return Observable.just(repos).paginate(nextPageTrigger: nextPageTrigger, hasNextPage: { _ in
            return true
        }, nextPageFactory: { _ in
            self.paginateRepoItems(page: page, nextPageTrigger: nextPageTrigger)
        })
    }
}
